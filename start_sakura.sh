#!/bin/bash


#: Title                : start sakura
#: Date                 : 26-02-2016
#: Modified             : 04-05-2016
#: Author               : "Florea Marius Florin" <florea.fmf@gmail.com>
#: Descprition          : if sakura already running switches to it, else
#:                      :   starts sakura maximized
#: Options              : none
#: Requirements         : sakura, pidof, xdotool
#: Notes                : This script is to be used only with a keyboard
#:                      :   accelerator


# get sakura pid
PID="$(pidof sakura)"

# temp file used to keep track of sakura window
tmpFile="/tmp/sakura_active_window"

# if it's running switch to it
if [ "$PID" ]; then
    # if it has focus then hide it
    hasFocusPID="$(xdotool getwindowfocus getwindowpid)"
    if [ "$PID" == "$hasFocusPID" ]; then
        wid="$(xdotool getactivewindow)"
        xdotool windowunmap "$wid"
        echo "$wid" > "$tmpFile"
    else
    # if it's hidden bring it to the front and switch to it
        wid="$(cat "$tmpFile")"
        xdotool windowmap "$wid"
        xdotool windowactivate "$wid"
    fi
else
# start sakura maximized
    sakura -m
fi
