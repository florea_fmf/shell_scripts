#!/bin/bash


#: Title				: Utility Functions
#: Date					: 15-03-2014
#: Modified				: 29-06-2016
#: Author				: "Florea Marius Florin" <florea.fmf@gmail.com>
#: Version				: 0.0-1 (???)
#: Description			: utility functions used to ease various tasks
#% It is meant to be sourced in .bashrc or equivalent. Not to be used
#% stand-alone.


#@ prints the file size for the given parameters
#- accepts as many arguments as given
function fsize() {
	du -ch "$@" | tail -n 1 | cut -f 1
}

#@ appends a copyright message to a binary files send as parameters;
#@ actually to all files, but it's meant just for fun, and the
#@   copyright isn't supposed to be seen;
function binary_copyright()
{
    copyright="fmf"

    for file in "$@"; do
        lastLine="$(tail -n 1 "$file")"

        if [[ "$lastLine" != "$copyright" ]]; then
            echo -e "\n$copyright" >> "$file"
        fi
    done
}


#@ helps with cheking for memory leaks by supplying some of the parameters
#@   to valgrind
function mem-check() {
    valgrind --error-limit=yes --read-var-info=yes \
        --log-file="mem-check-result" --leak-resolution=high \
        --leak-check=full "$@"
}


#@ removes ansi color codes from the given string
#- accepts one argument
#- 1: the string from which to remove the ansi color codes
function remove_ansi_color_codes() {
    IFS=""

    if [[ $# == 0 ]]; then
        # if the string is piped into the function, read it from stdin
        # prints an extra \n
        while read -r line; do
            str="$(printf "%s" "$line" | sed 's/\x1b\[[0-9;]*m'//g)"
            printf "%s\n" "$str"
        done
    else
        # the string is given as a function argument
        str="$(printf "%s" "$@" | sed 's/\x1b\[[0-9;]*m'//g)"
        printf "%s" "$str"
    fi
}

#@ deletes all the temporary files from user's home dir
#- accepts no arguments
function delete_temp_files_from_home() {
    find $HOME -name *~ -delete
}

#@ checks for duplicates lines in a file
#@ the input can not be piped in the function
#- accepts 1 argument
#- 1: path to the file to be checked
function dup_lines() {
    run1=$(cat "$1" | wc -l)
    run2=$(cat "$1" | sort | uniq -u | wc -l)

    if [[ $run1 != $run2 ]]; then
        printf "true\n"
    else
        printf "false\n"
    fi
}

#@ sets global volume
#- accepts 1 argument
#- 1: the volume to be set in percentage
function set_volume() {
    # we assume that sink 0 is the one from where sound is coming
    pactl set-sink-volume 0 "$1"%
}
