#!/bin/bash


#: Title					: gfid - Grep Files In Directories
#: Date						: 04-12-2013
#: Modified					: 04-12-2013
#: Author					: "Florea Marius Florin" <florea.fmf@gmail.com>
#: Version					: 0.4
#: Options					: 'input-dir' 'grep-args'
#: Arguments (mandatory)	:
#: 			- 1st argument is the directory where files to be searched are located
#: 			- 2nd, 3rd... nth arguments will be passed verbatim to grep
#: Optional Arguments		: -
#: Requirements				: find, file, cat, grep utilities
#: Additional Info			:
#: 			- grep is invoked with the '-n' arg: to print the line number where the match occured
#: 			- script doesn't check if the utilities required are installed
#: 			- only greps text files
#: 			- doesn't look in .git folders
#: 			- output might be misrepresentative since \t are stripped out of greps results


function show_help() {
	printf "This script requires at least 2 arguments.\n"
	printf "1st argument is the directory where the files to be grepped are.\n"
	printf "remaining arguments will be passed verbatim to grep.\n"
}


if [[ $# < 2 ]]; then
	show_help
	exit 1
fi


# get the dir and shift arguments by one so whatever remains is passed to grep
dir="$1"
shift
grepArgs="$@"

# get list with all the files in the given directory
files="$(find -L "$dir" -type f 2> /dev/null)"

while read -r file; do
	# check if the file is a text file and it's not in a .git dir
	# don't want to grep through git objects
	if [[ ! "$file" =~ ".git/"  ]]; then
		fileType="$(file "$file" | cut -d ':' -f 2,3,4,5)"
		if [[ "$fileType" =~ "ASCII text" ]]; then
			# it's a text file grep it
			res="$(cat "$file" | grep -n $grepArgs | tr -d '\t')"
			if [[ "$res" != "" ]]; then
				# we got a match for whatever we are looking for
				printf "%s:\n%s\n\n" "$file" "$res"
			fi
		fi
	fi
done <<< "$files"

exit 0
