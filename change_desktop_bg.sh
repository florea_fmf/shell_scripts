#!/bin/bash

#: Title        : change desktop background
#: Date         : 30-01-2015
#: Modified     : 30-01-2015
#: Author       : "Florea Marius Florin" <florea.fmf@gmail.com>
#: Version      : 1.0
#: Description  : Changes the desktop and lockscreen wallpapers to a random one
#:                chosen from a a user given directory after an user given
#:                time has passed
#: Requirements : gsettings;
#:                wallpapers must be png format

#% Setup:
#%  - put this script to be run at system start-up
#%  create a .desktop file in ~/.config/autostart named "change_desktop_bg.desktop"
#%  write this in it and save
#%  [Desktop Entry]
#%  Name=change_desktop_bg.sh
#%  GenericName=Change Desktop Background
#%  Comment=Changes the desktop and lockscreen backgrounds
#%  Exec=path_where_the_script_is_saved
#%  Terminal=false
#%  Type=Application
#%  X-GNOME-Autostart-enabled=true

#% Change this to suit your needs
WALLPAPERS="$HOME/Pictures/wallpapers"     # where the wallpapers are stored
INTERVAL=300                    # after how many seconds to change the wallpaper

# run indefinately
while [ 1 ]; do
    # get a random wallpaper
    randomWall="$(ls $WALLPAPERS/*.png | shuf -n 1)"
    # set the desktop and then the lockscreen background to the chosen picture
    gsettings set org.gnome.desktop.background picture-uri "file://$randomWall"
    gsettings set org.gnome.desktop.screensaver picture-uri "file://$randomWall"

    # repeat after this many seconds have passed
    sleep $INTERVAL
done

exit 0

