#!/bin/bash


#: Title                  : convert wallpapers
#: Date                   : 29-07-2015
#: Modified               : 29-07-2015
#: Author                 : "Florea Marius Florin" <florea.fmf@gmail.com>
#: Description            : converts the wallpapers found in INPUT_DIR to
#:                        :   png files 1366x768 in size. Saves them in
#:                        :   OUTPUT_DIR naming them in numerical order
#: Options                : none
#: Requirements           : imagemagick


# where the wallpapers to be converted are stored
INPUT_DIR="/home/fmf/Pictures/walls/"
# where to store the converted wallpapers
OUTPUT_DIR="/home/fmf/Pictures/wallpapers/"

# get the name of all the files in the input directory
inputFiles="$(ls $INPUT_DIR)"
read -a inputFilesArr <<< $inputFiles

# get the number of the last converted wallpaper
lastConvFile="$(ls "$OUTPUT_DIR" | sort -n | tail -n 1)"
lastConvFile=${lastConvFile%.*}
if [[ "$lastConvFile" == "" ]]; then
    lastConvFile=0
fi

# counters
totalFiles=0
convertedFiles=0
failedFiles=0

# convert each file from input dir to output dir
for file in ${inputFilesArr[@]}; do
    let "totalFiles++"
    let "lastConvFile++"
    convert $INPUT_DIR$file -resize 1920x1080\! $OUTPUT_DIR$lastConvFile".png"
    if [ $? -eq 0 ]; then
        let "convertedFiles++"
        printf "Converted: %s\n" "$file"
        # delete the original file
        rm -f "$INPUT_DIR$file"
    else
        let "failedFiles++"
        printf "Failed: %s\n" "$file"
    fi
done

# display result
echo "----"
printf "Total files: %d\n" $totalFiles
printf "Converted files: %d\n" $convertedFiles
printf "Failed files: %d\n" $failedFiles


exit
