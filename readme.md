my collection of (questionable) usefull shell scripts
=====================================================

Scripts
-------
Usable:

- ogg2mp3.sh -- converts ogg files to mp3 files
- fpc_color_output.sh -- colorize the output of fpc
- utility_funcs -- usefull funcs to be put in .bashrc
- change_desktop_bg -- changes the desktop and lockscreen background
- convert_walls.sh  -- converts wallpapers
- start_sakura.sh -- switches to sakura terminal if already running or starts
                     a new instance of it

More or less shit:

- gfid.sh -- greps through all files in a given directory

License
-------
Copyright (C) 2013-2016 Florea Marius Florin

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented: you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.

Florea Marius Florin, florea.fmf@gmail.com
