#!/bin/bash


#: Title					: ogg2mp3
#: Date						: 18-11-2013
#: Modified					: 24-04-2014
#: Author					: "Florea Marius Florin" <florea.fmf@gmail.com>
#: Version					: 2.0.1
#: Description				: converts ogg files to mp3 files
#: Options					: -i 'input dir' -o 'output-dir' -q '(number)'
#:								[-h] [-y] [--vbr] [--no-recursive] [--one-dir]
#:                              [-w]
#: Options (mandatory)		:
#:		- -i | --input		: path to where the ogg files are.
#:		- -o | --output		: path where to save the mp3 files.
#:		- -q | --quality	: the bitrate mp3s will be encoded. Or the mp3s
#:								quality if the --vbr switch is given
#: Options (optional)		:
#:		- -h | --help		: prints a help msg and quits.
#:		- -y | --overwrite	: if output-dir already exists will overwrite the
#:								files found there. If given this switch will
#:								not prompt the user for permission.
#:      - -w | --windows    : writes tags that can be read by windows.
#:		- --vbr				: encodes mp3s using variable bitrate. Default
#:								is constant bitrate.
#:		- --no-recursive	: doesn't go into subdirectories. Converts only
#:								the ogg files in the input-dir.
#:		- --one-dir			: saves all mp3 files in the output-dir. Doesn't
#:								create subdirectories even if they exist in
#:								input-dir.
#% All the arguments that don't match any of the above are silently ignored.
#% If the script is interrupted before it can finnish then some junk
#%	files can remain in /tmp:
#%		- the cover art image
#%		- 2 empty text files with the name: tmp.'random letters and numbers'
#%	These files does not affect subsequent runs of the script.
#% It saves the cover art only if it was embeded in the ogg file using
#%	the old method with COVERART.
#% It saves only the following tags (if present in the ogg file):
#%	- title
#%	- artist
#%	- album
#%	- year
#%	- track number
#%	- genre

#% Parameters that can be changed by the user
#% properties of the image to embed into mp3 files
IMAGE_FORMAT=".jpg"			# jpg or png
IMAGE_QUALITY="100"			# [0-100]
IMAGE_SIZE="300x300"		# image size in pixels (WIDTHxHEIGHT)
# if true it will print the full path to the song it currently converts
# if false it prints just the song's name
FULL_PATH=true

#% global vars (setted to default values, values  will be given or overwritten
#%   when cli args are parsed)
SCR_NAME="${0##*/}"			# get the script name (might be changed by user)
INPUT_DIR=
OUTPUT_DIR=
QUALITY=
WINDOWS=false               # default is to write the newest version of tags
VBR=false					# default is to use constant bitrate
RECOURSE=true				# default is to recourse in subdirectories
ONE_DIR=false				# default is to create subdirectories
OVERWRITE=false				# default is not to overwrite output-dir
LINES=						# the terminal number of lines
COLS=						# the terminal number of columns
HAS_NOTIFY=					# true if notify-send is installed
CONVERSION_STATUS="success"	# tells if the conversion succeded or failed
							#	it will be changed to "failed" when required
# length of the status bar. If the screen width < STATUS_BAR_LEN then
#	showing the status bar messes up the output. So in this case we
#	disable the status bar.
# Note: the actual length is: 46+2*length(totalNumberOfSongsToConvert)
#	you MUST adjust to this when the totalNumberOfSongsToConvert is known
STATUS_BAR_LEN=46


#% functions used in the script
#@ exits script prematurely
function exit_interrupted() {
	pkill -P $$				# kill spawned subprocesses
	restore_terminal "NULL" "NULL"
	printf "The script was interrupted. Its output is likely unusable.\n" >&2
	printf "Some junk files might have been left behind in /tmp.\n" >&2
	exit 0
}

#@ checks if required programs are installed or are the correct version
#@ Note: it doesn't check for every program this script calls. Only those
#@	that are more likely not to be installed.
function check_requirements() {
	# bash version ( >= 4)
	if (( BASH_VERSINFO[0] < 4 )); then
		printf "This script requires at least BASH Version 4.\n" >&2
		exit 1
	fi

	# ffmpeg
	type ffmpeg &> /dev/null
	if (( $? != 0 )); then
		printf "This script requires 'ffmpeg' to be installed.\n" >&2
		exit 1
	fi

	# vorbiscomment
	type vorbiscomment &> /dev/null
	if (( $? != 0 )); then
		printf "This script requires 'vorbiscomment' to be installed.\n" >&2
		printf "It is usually found in 'vorbis-tools' package.\n" >&2
		exit 1
	fi

	# convert
	type convert &> /dev/null
	if (( $? != 0 )); then
		printf "This script requires 'convert' to be installed.\n" >&2
		printf "It is usually found in 'imagemagick' package.\n" >&2
		exit 1
	fi

	# base64
	type base64 &> /dev/null
	if (( $? != 0 )); then
		printf "This script requires 'base64' to be installed.\n" >&2
		exit 1
	fi

	# tput
	type tput &> /dev/null
	if (( $? != 0 )); then
		printf "This script requires 'tput' to be installed.\n" >&2
		exit 1
	fi

	# notify-send; note this is not required. It's only used to
	#	"enhance" user experience
	type notify-send &> /dev/null
	if (( $? == 0 )); then
		HAS_NOTIFY=true
	else
		HAS_NOTIFY=false
	fi
}

#@ prints the help
function print_help() {
cat << EOF
Usage
    "$SCR_NAME" -i "input-dir" -o "output-dir" -q "number" [-h] [-y] [--vbr]
        [--no-recursive] [--one-dir]

    Mandatory options:
        -i, --input="DIRECTORY"
            Path to the directory where the files to be converted are.
        -o, --output="DIRECTORY"
            Path where the converted files should be saved.
        -q, --quality="NUMBER"
            The quality for the resulting mp3s.

    Optional options:
        -h, --help
            Prints this help and exits.
        -y, --overwrite
            Overwrite all the files in the output directory if they exists.
        -w, --windows
            Writes an older version of mp3 tags that can be read be windows.
        --vbr
            Use variable bitrate when converting the files.
        --no-recursive
            Don't go in subdirectories when converting files.
        --one-dir
            Don't create subdirectories in the output directory.

    Note:
        - All the options/arguments that don't match any of the above
            will be silently ignored.
		- Quality must be a number in the range [0-320]. Or [0-9] if --vbr
            is used.
        - Input-dir and output-dir can NOT overlap.
        - If the output-dir doesn't exists this script will create it.
EOF
}

#@ parse command line arguments
#- takes 1 argument.
#- 1: the script parameter list ("$@")
function parse_args() {
	# call getopt
	local args=`getopt -q -o "i:o:q:hyw" \
		-l "input:,output:,quality:,help,vbr,no-recursive,one-dir,overwrite,windows" \
		-n "$SCR_NAME" -- "$@"`

	# magic
	eval set -- "$args"

	# get the options
	while true; do
		case "$1" in
			-i | --input)
				INPUT_DIR="$2"
				shift 2
				;;
			-o | --output)
				OUTPUT_DIR="$2"
				shift 2
				;;
			-q | --quality)
				QUALITY="$2"
				shift 2
				;;
			-y | --overwrite)
				OVERWRITE=true
				shift
				;;
			-h | --help)
				print_help
				exit 0
				;;
            -w | --windows)
                WINDOWS=true
                shift
                ;;
			--vbr)
				VBR=true
				shift
				;;
			--no-recursive)
				RECOURSE=false
				shift
				;;
			--one-dir)
				ONE_DIR=true
				shift
				;;
			--)
				shift
				break
				;;
		esac
	done

	# get the terminal capabilities
	LINES="$(tput lines)"
	COLS="$(tput cols)"
}

#@ checks arguments for sane values
function check_args() {
	# check if input dir is given and it exists
	if [[ "$INPUT_DIR" == "" ]]; then
		printf "The input directory is required.\n" >&2
		exit 1
	elif [ ! -d "$INPUT_DIR" ]; then
		printf "The input directory doesn't exists" >&2
		printf " or is not a directory.\n" >&2
		exit 1
	fi

	# check if output dir is given and if it exists to overwrite it
	#	or create it if it doesn't exists
	if [[ "$OUTPUT_DIR" == "" ]]; then
		printf "The output directory is required.\n" >&2
		exit 1
	elif [ -d "$OUTPUT_DIR" ] && [[ $OVERWRITE == false ]]; then
		printf "Output directory already exists.\n"
		printf "Do you want to overwrite it? (y/n)\n"
		read -r ans
		if [[ "${ans^^}" == "Y" ]]; then
			OVERWRITE=true
		else
			exit 0
		fi
	else
		mkdir -p "$OUTPUT_DIR"
	fi

	# check if quality is a number and is in the expected range
	if [[ "$QUALITY" == "" ]]; then
		printf "Quality is required.\n" >&2
		exit 1
	elif [[ "$QUALITY" =~ ^[0-9]+$ ]]; then
		if [[ $VBR == false ]]; then
			# negative values are removed by regex
			if (( $QUALITY > 320 )); then
				printf "Quality is not in the expected range [0-320].\n" >&2
				exit 1
			fi
		else
			if (( $QUALITY > 9 )); then
				printf "Quality is not in the expected range [0-9].\n" >&2
				exit 1
			fi
		fi
	else
		printf "Quality is not a positive number.\n" >&2
		exit 1
	fi

	# add '/' at the end of paths if it isn't already there
	if [[ "${INPUT_DIR: -1}" != "/" ]]; then
		INPUT_DIR="$INPUT_DIR/"
	fi

	if [[ "${OUTPUT_DIR: -1}" != "/" ]]; then
		OUTPUT_DIR="$OUTPUT_DIR/"
	fi
}

#@ formats seconds as hh:mm:ss
#- accepts 1 argument
#- 1: the number of seconds as an integer number
function format_seconds() {
	local h="$(( $1 / 3600 ))"
	local m="$(( ($1 / 60) % 60 ))"
	local s="$(( $1 % 60 ))"

	printf "%02d:%02d:%02d" "$h" "$m" "$s"
}

#@ notify the user via system notifications how and when the
#@	converting has finnished
#- accepts 1 argument
#- 1: 'success' or 'failure': to tell if the converting finnished
#-		succesfully or failed
function notify() {
	if [[ "${1,,}" == "success" ]]; then
		local msg="Conversion finnished succesfully."
		echo_bold "$msg"
		if [[ $HAS_NOTIFY == true ]]; then
			notify-send --urgency=normal --expire-time=3000 \
				"$SCR_NAME" "$msg"
		fi
	elif [[ "${1,,}" == "failure" ]]; then
		local msg="Conversion failed."
		echo_bold "$msg"
		if [[ $HAS_NOTIFY == true ]]; then
			notify-send --urgency=critical --expire-time=3000 \
				"$SCR_NAME" "$msg"
		fi
	fi
}

#@ prints the curent song it converts making sure it doesn't exceeds
#@	the screen width
#% if the string is bigger than the screen width then the string is
#%	modified to fit in the specified width: keeps the beginning of string,
#%	replaces the middle with '...' and appends the end of string
#%	e.g.: str="abcdefghijklmnopqrs"
#%	output: "abc...qrs"
#% Note: actual output is "Converting: song name", where the song name
#% is the function parameter and "Converting: " is printed in bold
#- accepts 1 parameter
#- 1: the song name to be printed
function print_current_song {
	local strHalf="(( ($COLS-12)/2-1 ))"
	local songName=						# the song name
	local songLen="${#1}"
	songLen="(( $songLen+12 ))"			# to account for "Converting: "

	# modify the song name to fit screen width (if necessary)
	if (( $songLen > $COLS )); then
		songName="${1:0:$strHalf}...${1: -(( $strHalf-1 ))}"
	else
		songName="$1"
	fi

	# print the message
	# delete until eoln; so current output doesn't interfere with previous
	tput el
	echo_bold "Converting: " "suppress"
	printf "%s\n" "$songName"
}

#@ prints the argument in bold
#- accepts 2 arguments
#- 1: the string to be printed in bold
#- 2: [optional] 'suppress': if given doesn't output the new line (\n)
function echo_bold() {
	tput bold				# makes text bold

	if [[ "${2,,}" == "suppress" ]]; then
		printf "%s" "$1"
	else
		printf "%s\n" "$1"
	fi

	tput sgr0				# turn off text attributes
}

#@ print the argument with the colors reversed
#- accepts 2 argument
#- 1: the string to be printed with the colors reversed
#- 2: [optional] 'suppress': if given doesn't output the new line (\n)
function echo_inverted() {
	tput smso				# turn reverse mode on

	if [[ "${2,,}" == "suppress" ]]; then
		printf "%s" "$1"
	else
		printf "%s\n" "$1"
	fi

	tput rmso				# turn reverse mode off
}

#@ show the progress bar at the bottom of the terminal screen
#- accepts 3 arguments
#- 1: path to a file that acts as the runningFlag*
#- 2: how many songs have been converted
#- 3: total number of songs
#% *: the 'runningFlag' is an empty file (usually in /tmp or where mktemp makes
#%		it) which signals that ffmpeg is still converting. Basically it tells
#%		the function to update the status bar as long as this flag exists.
function show_progress() {
	sleep 0.2					# otherwise fucks up the output
	while [ -f "$1" ]; do
		tput sc					# save cursor position
		tput cup $LINES 0		# move cursor to last line

		local elapsed="$(format_seconds $SECONDS)"
		local progress="$(( ($2 - 1) * 100 / $3 ))%"
		local msg=" Progress: [ $2 ]/[ $3 ] -- Time: $elapsed - $progress "
		echo_inverted "$msg" "suppress"			# tell user overall progress

		tput rc					# restore cursor position
		sleep 0.5
	done
}

#@ restores the terminal window in a usable state
#- accepts 2 arguments
#- 1: how many songs have been converted
#- 2: total number of songs
function restore_terminal() {
	if (( $COLS >= $STATUS_BAR_LEN )); then
		tput sc						# save cursor position
		tput cup $LINES 0			# move cursor to last line
		tput el						# delete until eoln
		tput rc						# restore cursor position
		local msg=" Progress: [ $1 ]/[ $2 ] -- Time: "
		msg="$msg""$(format_seconds $SECONDS) - 100% "
		echo_inverted "$msg"
	fi
}

#@ converts one song from ogg to mp3
#- accepts 3 arguments
#- 1: path to the runningFlag (see show_progress())
#- 2: path to the ogg song
#- 3: path to the folder where the converted song should be placed
function convert_song() {
	# tell the user at what song we are
	if [[ $FULL_PATH == true ]]; then
		print_current_song "$2"
	else
		print_current_song "${2##**/}"
	fi

	local name="${2##**/}"		# get the file name and extension
	name="${name%.*}"".mp3"		# replace extension

	# get song metadata
	# store the full song metadata in a temp file and then use grep to get
	#	the pieces we're interested in.
	local metadata="$(mktemp)"
	vorbiscomment "$2" > "$metadata"

	# check if conversion was succesfull
	# a little severe to bail out if we can't extract the song metadata,
	#	but usually is because the file is not ogg.
	if (( $? != 0 )); then
		CONVERSION_STATUS="failed"
	fi

	local title="$(grep '^TITLE=' "$metadata" | cut -d '=' -f 2)"
	local artist="$(grep '^ARTIST=' "$metadata" | cut -d '=' -f 2)"
	local album="$(grep '^ALBUM=' "$metadata" | cut -d '=' -f 2)"
	local year="$(grep '^DATE=' "$metadata" | cut -d '=' -f 2)"
	local track="$(grep '^TRACKNUMBER=' "$metadata" | cut -d '=' -f 2)"
	local genre="$(grep '^GENRE=' "$metadata" | cut -d '=' -f 2)"
	local imageData="$(grep '^COVERART=' "$metadata" | cut -d '=' -f 2)"
	local imageName="/tmp/""$(grep '^COVERARTDESCRIPTION=' "$metadata" \
		| cut -d '=' -f 2)"
	# if the images are the same: name.extension convert fails, and also makes
	#	ffmpeg to fail, resulting in nothing getting converted;
	# we alter the name of coverName a little, and after the imageName
	#	(original image) is deleted we change the name of coverName to
	#	that of the original;
	local coverName="${imageName%.*}""x""$IMAGE_FORMAT"

	if [[ "$imageData" != "" ]]; then	# it has album cover
		# make picture from imageData
		printf "%s" "$imageData" | base64 --decode > "$imageName" 2> /dev/null

		# resize and convert the image to given properties
		convert -quiet "$imageName" -quality "$IMAGE_QUALITY" -resize \
			"$IMAGE_SIZE" "$coverName"

		# delete the original image and rename the coverName
		rm -f "$imageName"
		mv -f "$coverName" "${imageName%.*}""$IMAGE_FORMAT"
		coverName="${imageName%.*}""$IMAGE_FORMAT"
	fi

	# build the ffmpeg arguments based on our settings
	local ffArgs="-loglevel quiet -y -i \""$2"\" "

    # for tags to be seen by windows an older version is required to be written
    if [[ $WINDOWS == true ]]; then
        ffArgs="$ffArgs""-id3v2_version 3 -write_id3v1 1 "
    fi

	if [[ "$imageData" != "" ]]; then
		ffArgs="$ffArgs""-i \""$coverName"\" -map 0:0 -map 1:0 -c copy "

        if [[ $WINDOWS == true ]]; then
            ffArgs="$ffArgs""-id3v2_version 3"
        else
		    ffArgs="$ffArgs""-id3v2_version 4 "
        fi

		ffArgs="$ffArgs""-metadata:s:v title=\""$(basename "$coverName")"\" "
		ffArgs="$ffArgs""-metadata:s:v comment=\"Cover (front)\" "
	fi

	if [[ $VBR == false ]]; then
		ffArgs="$ffArgs""-acodec mp3 -ab "$QUALITY"k "
	else
		ffArgs="$ffArgs""-acodec mp3 -q:a "$QUALITY" "
	fi

	ffArgs="$ffArgs""-metadata TIT2=\""$title"\" "
	ffArgs="$ffArgs""-metadata TPE1=\""$artist"\" "
	ffArgs="$ffArgs""-metadata TPE2=\""$artist"\" "
	ffArgs="$ffArgs""-metadata TALB=\""$album"\" "
	ffArgs="$ffArgs""-metadata TDRC=\""$year"\" "
	ffArgs="$ffArgs""-metadata TRCK=\""$track"\" "
	ffArgs="$ffArgs""-metadata TCON=\""$genre"\" "
	ffArgs="$ffArgs""\""$3/$name"\""

	# convert the file
	printf "%s" "$ffArgs" | xargs ffmpeg

	# check if conversion was succesfull
	if (( $? != 0 )); then
		CONVERSION_STATUS="failed"
	fi

	# clean up
	rm -f "$metadata"
	rm -f "$coverName"
	# ffmpeg finnished converting. Remove the runningFlag so show_progress()
	#	can exit
	rm -f "$1"
}

#@ converts the songs
function convert_songs() {
	local currentSong=0		# keeps count of how many songs have been converted

	if [[ $RECOURSE == true ]]; then
		# get the list with the files to be converted
		local files="$(find -L "$INPUT_DIR" -name *.ogg | sort -d)"

		# get the number of files
		local songs="$(printf "%s" "$files" | wc -l)"
		if (( $songs == 0 )); then
			printf "Nothing to convert.\n"
			exit 0;
		else
			# it loses the last song, since wc counts only \n
			(( songs++ ))
		fi

		# get the real status bar length; because we know the number of songs
		STATUS_BAR_LEN="(( $STATUS_BAR_LEN+2*${#songs} ))"

		# convert the files
		local lastDir=				# used to see if we are in another dir
		local currentDir=			# the dir we currently are converting from
		local outputDir=			# where the songs will be saved
		while read -r file; do
			currentDir="${file%/*g}"

			# we have finnished all songs from the previous dir
			#	and entered a new one
			if [[ $ONE_DIR == true ]]; then
				outputDir="$OUTPUT_DIR"
			elif [[ "$currentDir" != "$lastDir" ]]; then
				lastDir="$currentDir"
				outputDir="$OUTPUT_DIR""${currentDir#**/}"
				mkdir -p "$outputDir"
			fi

			(( currentSong++ ))
			local runningFlag="$(mktemp)"		# create the flag

			# show the progress bar only if it fits on screen
			if (( $COLS >= $STATUS_BAR_LEN )); then
				show_progress "$runningFlag" "$currentSong" "$songs" &
			fi

			convert_song "$runningFlag" "$file" "$outputDir"

			# no point going on when a song failed to convert
			if [[ "$CONVERSION_STATUS" != "success" ]]; then
				break
			fi

			# needed only if the progress bar is shown
			if (( $COLS >= $STATUS_BAR_LEN )); then
				wait
			fi
		done <<< "$files"
	else
		# get the list with the files to be converted
		local files="$(find -L "$INPUT_DIR" -maxdepth 1 -name *.ogg | sort -d)"

		# get the number of files in the input folder
		local songs="$(printf "%s" "$files" | wc -l)"
		if (( $songs == 0 )); then
			printf "Nothing to convert.\n"
			exit 0;
		else
			# it loses the last song, since wc counts only \n
			(( songs++ ))
		fi

		# create the output dir/dirs
		if [[ $ONE_DIR == true ]]; then
			local outputDir="$OUTPUT_DIR"
		else
			local outputDir="$OUTPUT_DIR${INPUT_DIR#**/}"
			mkdir -p "$outputDir"
		fi

		# get the real status bar length; because we know the number of songs
		STATUS_BAR_LEN="(( $STATUS_BAR_LEN+2*${#songs} ))"

		# convert the songs
		while read -r file; do
			(( currentSong++ ))
			local runningFlag="$(mktemp)"			# create the flag

			# show the progress bar only if it fits on screen
			if (( $COLS >= $STATUS_BAR_LEN )); then
				show_progress "$runningFlag" "$currentSong" "$songs" &
			fi

			convert_song "$runningFlag" "$file" "$outputDir"

			# no point going on when a song failed to convert
			if [[ "$CONVERSION_STATUS" != "success" ]]; then
				break
			fi

			# needed only if the progress bar is shown
			if (( $COLS >= $STATUS_BAR_LEN )); then
				wait
			fi
		done <<< "$files"
	fi

	# restore terminal to a usable state
	restore_terminal "$currentSong" "$songs"
}

#% main body of script
# catch interrupt signals
trap exit_interrupted SIGINT
trap exit_interrupted SIGTERM

# check if the programs we need are installed
check_requirements

# get arguments
parse_args "$@"
check_args

# convert the songs
convert_songs

# notify the user how was the conversion
if [[ "$CONVERSION_STATUS" == "success" ]]; then
	notify "success"
else
	notify "failure"
fi

exit 0
