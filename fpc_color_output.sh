#!/bin/bash


#: Title				: fpc color output
#: Date					: 05-01-2014
#: Modified				: 30-01-2015
#: Author				: "Florea Marius Florin" <florea.fmf@gmail.com>
#: Version				: 1.0.2
#: Description			: colorize output of fpc in (somewhat) the same
#:						:   manner as clang
#: Options				: none
#: Requirements			: existance of variable FPC_COLOR;
#:						    with one of the values: TRUE, FALSE, TRUE-SUPPRESS
#: 						: this file should be sourced before calling fpc

#% Setup:
#%   - in your .bashrc (or equivalent rc file) export the variable FPC_COLOR
#%       export FPC_COLOR="TRUE"			# to enable color output
#%       export FPC_COLOR="FALSE"			# to disable color output
#%       export FPC_COLOR="TRUE-SUPPRESS"	# to enable selective color output
#%   - source this script file:
#%       source "path to this script"
#%   - note:
#%     if you call fpc from another script file you need to source this file
#%       again in that script to get color output
#%
#%   -- example:
#%     if this file is saved in ~/bin then add this to .bashrc
#%       export FPC_COLOR="TRUE"
#%       source ~/bin/fpc_color_output.sh
#%     while FPC_COLOR is TRUE; fpc output will be colorized when it's called


#% convenience functions
#@ print the next lines with the given color
#@ also makes the text bold
#- accepts 1 parameter: the color to change to
function _set_color() {
    tput bold
    tput setaf "$1"
}

#@ resets the text color to default
function _clear_color() {
	tput sgr0
}

#@ colorize the word in the message with the given color
#@ or prints the message as is if the word isn't found
#@ and the 'suppress' parameter is not given
#- function accepts 4 parameters (first 3 params are mandatory)
#- 1st: the message to be printed
#- 2nd: the word to be printed in different color
#- 3rd: the color in which to print the word (number: 0-7)
#- 4th: 'suppress' if given and the 'word' is not found in 'message'
#-      doesn't print anything
function _print_word_colorized() {
	local msg="$1"
	local word="$2"
	local color="$3"
	local suppress="$4"

	if [[ "$msg" =~ "$word" ]]; then
		printf "%s" "${msg%%$word*}"		# print what is before 'word'
		_set_color "$color"
		printf "%s" "$word"					# print the word
		_clear_color
		printf "%s\n" "${msg##*$word}"		# print what is after 'word'
	elif [[ "$suppress" != "suppress" ]]; then		# mind == blown
		printf "%s\n" "$msg"
	fi
}


#@ colorize the output of fpc to be easier to recognise important messages;
#@ output of fpc must be piped into the function to work;
#- function accepts 1 argument "suppress"
#-   if given the function will print only the important stuff
#-   else the function will print all the output fpc gives
function _colorize() {
	# available colors: 0 - Black; 1 - Red; 2 - Green; 3 - Yellow;
	#   4 - Blue; 5 - Magenta; 6 - Cyan; 7 - White
	# colors used to display compiler's messages
	local HINT=6
	local ERROR=1
	local WARNING=3
	local OPTINFO=2
	local TIME=5				# compiling time
	local LINKER=4				# color for ld messages

	# fpc uses full ld path to print messages from it
	local ldPath="$(which ld)"

	# words to be highlighted
	declare -a highWords=("Hint" "Error" "Fatal" "Warning" "Note" \
		"Linking" "Compiling" "$ldPath")

	# color for the highlighted words
	declare -A highColor
	highColor["${highWords[0]}"]="$HINT"			# hint
	highColor["${highWords[1]}"]="$ERROR"			# error
	highColor["${highWords[2]}"]="$ERROR"			# fatal
	highColor["${highWords[3]}"]="$WARNING"			# warning
	highColor["${highWords[4]}"]="$WARNING"			# note
	highColor["${highWords[5]}"]="$OPTINFO"			# linking
	highColor["${highWords[6]}"]="$OPTINFO"			# compiling
	highColor["${highWords[7]}"]="$LINKER"			# ld

	while read line; do
		# to avoid printing the same line more than 1 time
		local printed=0

		for word in "${highWords[@]}"; do
            # skip empty lines
            if [[ "$line" == "" ]]; then
                printed=1
                break
            elif [[ "$line" =~ "$word" ]]; then
				_print_word_colorized "$line" "$word" ${highColor[$word]}
				printed=1
				break
			elif [[ "$line" =~ "lines compiled" || "$line" =~ "issued" ]]; then
				_set_color "$TIME"			# print the whole line with
				printf "%s\n" "$line"		# this color
				_clear_color
				printed=1
				break
			fi
		done

		if [[ "$1" != "suppress" && "$printed" == "0" ]]; then
			printf "%s\n" "$line"
		fi
	done
}


#@ if the variable FPC_COLOR is set to TRUE then colorize fpc output
#@ if FPC_COLOR is set to TRUE-SUPPRESS colorize compiler output and
#@   discard (what I deem as) unimportant messages: messages that don't
#@   contain one of the word in the 'highWords' array
function fpc() {
	local fpc_compiler=$(which fpc)

	# ld outputs to stderr; redirect stderr to stdout to catch that aswell
	if [[ "$FPC_COLOR" == "TRUE" ]]; then
		$fpc_compiler "$@" 2>&1 | _colorize
	elif [[ "$FPC_COLOR" == "TRUE-SUPPRESS" ]]; then
		$fpc_compiler "$@" 2>&1 | _colorize "suppress"
	else
		$fpc_compiler "$@"
	fi
}
